package com.example.githubviewer.model;

import android.content.Context;
import android.graphics.Bitmap;
import android.widget.ImageView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.ImageRequest;
import com.example.githubviewer.model.Exceptions.DownLoadException;
import com.example.githubviewer.model.Exceptions.HTTPDownloadException;
import com.example.githubviewer.model.Requests.UsersRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Represents server GitHub repository.
 */
public class GithubRepository {

    private static GithubRepository instance;

    /** Url to get first group of users. */
    private final String FIRST_USERS_URL = "https://api.github.com/users?since=1";
    /** Url to get user info */
    private final String USER_URL = "https://api.github.com/users/";

    /** Pattren to find next url. */
    private final Pattern nexUrlPatt = Pattern.compile("<(.*)>; rel=\\\"next\\\"");

    /** Url for next parts of users. */
    private String nextURL = null;
    /** Wrapper to get queue. */
    private ResponseListener responseListener;
    /** Tag to mark all requests. */
    private Object tag;
    private Context context;
    /** Users Downloading is in process. */
    private boolean isUsersDownloading = false;
    /** User Downloading is in process. */
    private boolean isUserDownloading = false;
    /** There is no more feed on server. */
    private boolean feedEnded = false;

    /**
     * Interface to return results from server.
     */
    public interface ResponseListener {

        void onUsersResponse(ArrayList<User> users);

        void onUserResponse(User user);

        void onError(DownLoadException e);

    }

    public static GithubRepository getInstance(Context context, ResponseListener listener) {
        if (GithubRepository.instance == null) {
            GithubRepository.instance = new GithubRepository(context, context, listener);
            return  GithubRepository.instance;
        } else {
            String nextUrl = GithubRepository.instance.nextURL;
            GithubRepository.instance = new GithubRepository(context, context, listener);
            GithubRepository.instance.nextURL = nextUrl;
            return  GithubRepository.instance;
        }
    }

    private GithubRepository(Object tag
            , Context context, ResponseListener listener) {
        this.responseListener = listener;
        this.tag = tag;
        this.context = context;
    }

    public void setResponseListener(ResponseListener responseListener) {
        if (!(responseListener instanceof ResponseListener))
            throw new RuntimeException(responseListener.toString()
                + " must implement OnListFragmentInteractionListener");
        this.responseListener = responseListener;
    }

    /** Get next users from server. */
    public void nextUsers() {
        if (!this.isUsersDownloading) {
            // Exits if feed is exhausted
            if (this.feedEnded){
                this.responseListener.onError(new DownLoadException("No more feed"));
                return;
            }
            this.isUsersDownloading = true;
            String url = this.nextURL == null ? this.FIRST_USERS_URL : this.nextURL;
            UsersRequest request =
                    new UsersRequest(Request.Method.GET, url
                            , null, null, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                parseUsersResponse(response);
                            } catch (JSONException e) {
                                responseListener.onError(new DownLoadException());
                            } finally {
                                isUsersDownloading = false;
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if (error.networkResponse != null)
                                responseListener.onError(new HTTPDownloadException(error.networkResponse.statusCode));
                            else
                                responseListener.onError(new DownLoadException());
                        }
                    });
            request.setTag(this.tag);
            QueueSingleton.getInstance(this.context).addToRequestQueue(request);
        }
    }

    private void loadPictue(String url, Response.Listener<Bitmap> listener)  {
        ImageRequest request = new ImageRequest(url, listener, 0, 0
                , ImageView.ScaleType.CENTER_CROP, null, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error.networkResponse != null)
                    responseListener.onError(new HTTPDownloadException(error.networkResponse.statusCode));
                else
                    responseListener.onError(new DownLoadException());
            }
        });
        request.setTag(this.tag);
        QueueSingleton.getInstance(this.context).addToRequestQueue(request);
    }

    /** Parse response with list off users from server. */
    private void parseUsersResponse(JSONObject response) throws JSONException {
        // get data from response
        JSONArray jsonUsers = response.getJSONArray("data");
        ArrayList<User> users = new ArrayList();
        for (int i = 0; i < jsonUsers.length(); i++){
            final User user = new User(jsonUsers.getJSONObject(i).getString("login"));
            this.loadPictue(jsonUsers.getJSONObject(i).getString("avatar_url")
                    , new Response.Listener<Bitmap>() {
                        @Override
                        public void onResponse(Bitmap response) {
                            user.setAvatar(response);
                        }
                    });
            users.add(user);
        }
        // get url of next users
        this.nextURL = this.parseNextURL(response.getJSONObject("headers"));
        if (this.nextURL == null)
            this.feedEnded = true;
        // send response
        this.responseListener.onUsersResponse(users);
    }

    /** Parse url of next users. */
    private String parseNextURL(JSONObject headers) throws JSONException {
        Matcher matcher = this.nexUrlPatt.matcher(headers.getString("Link"));
        if (matcher.find())
            return matcher.group(1);
        else
            return "";
    }

    public void loadUser(final User user) {
        if (!this.isUserDownloading) {
            this.isUserDownloading = true;
            String url = this.USER_URL + user.login;
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url
                    , null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try{
                        parseUserResponse(response, user);
                    } catch (JSONException e) {
                        responseListener.onError(new DownLoadException());
                    } finally {
                        isUserDownloading = false;
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error.networkResponse != null)
                        responseListener.onError(new HTTPDownloadException(error.networkResponse.statusCode));
                    else
                        responseListener.onError(new DownLoadException());
                }
            });
            request.setTag(this.tag);
            QueueSingleton.getInstance(this.context).addToRequestQueue(request);
        }
    }

    public void parseUserResponse(JSONObject response, User user) throws JSONException {
        String name = this.getStrValue(response, "name");
        String company = this.getStrValue(response, "company");
        String location = this.getStrValue(response, "location");
        String email = this.getStrValue(response, "email");
        int reposCount = response.getInt("public_repos");
        int followersCount = response.getInt("followers");
        String creationDate = this.getStrValue(response, "created_at").substring(0, 10);
        String bio = this.getStrValue(response, "bio");
        user.setName(name).setCompany(company).setLocation(location)
                .setEmail(email).setReposCount(reposCount).setFollowersCount(followersCount)
                .setCreationDate(creationDate).setBio(bio);
        this.responseListener.onUserResponse(user);
    }

    /**
     * Return string value json object by key.
     * @param obj json object
     * @param key key
     * @return emty string if value is null, value otherwise.
     * @throws JSONException
     */
    private String getStrValue(JSONObject obj, String key) throws JSONException {
        if (obj.isNull(key))
            return "";
        else
            return obj.getString(key);
    }

    /** Cancel all previous requests. */
    public void cancelRequests() {
        QueueSingleton.getInstance(this.context).getRequestQueue().cancelAll(this.tag);
    }
}
