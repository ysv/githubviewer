package com.example.githubviewer.model.Exceptions;

public class HTTPDownloadException extends DownLoadException {

    private int status;

    public HTTPDownloadException(int status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Downloading has failed with status code" + this.status;
    }
}
