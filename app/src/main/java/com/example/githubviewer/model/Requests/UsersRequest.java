package com.example.githubviewer.model.Requests;

import android.support.annotation.Nullable;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.Map;

public class UsersRequest extends JsonObjectRequest {

    private Map<String, String> headers;

    public UsersRequest(int method, String url, Map<String, String> headers
            , @Nullable JSONObject jsonRequest
            , Response.Listener<JSONObject> listener
            , @Nullable Response.ErrorListener errorListener) {
        super(method, url, jsonRequest, listener, errorListener);
        this.headers = headers;
    }

    public UsersRequest(String url, Map<String, String> headers
            , @Nullable JSONObject jsonRequest
            , Response.Listener<JSONObject> listener
            , @Nullable Response.ErrorListener errorListener) {
        super(url, jsonRequest, listener, errorListener);
        this.headers = headers;
    }

    @Override
    public Map<String, String> getHeaders()throws AuthFailureError {
        return this.headers != null ? this.headers : super.getHeaders();
    }

    @Override
    protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
        try {
            // Convert bytes to str
            String json = new String(response.data
                    , HttpHeaderParser.parseCharset(response.headers));
            // Create json response
            JSONObject jsonResponse = new JSONObject();
            // Convert response headers
            jsonResponse.put("headers", new JSONObject(response.headers));
            // Convert response body
            jsonResponse.put("data", new JSONArray(json));
            return Response.success(jsonResponse
                    , HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JSONException e) {
            return Response.error(new ParseError(e));
        }
    }
}
