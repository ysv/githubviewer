package com.example.githubviewer.model;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class QueueSingleton {

    private static QueueSingleton instance;
    /** Application context */
    private static Context ctx;

    /** Queue to handle requests to server. */
    private RequestQueue requestQueue;

    /**
     * Return instance of queue wrapper.
     * If wrapper doesn't exist return new one else return existing one.
     * @param context current context
     */
    public static synchronized QueueSingleton getInstance(Context context) {
        if (QueueSingleton.instance == null)
            QueueSingleton.instance = new QueueSingleton(context);
        return QueueSingleton.instance;
    }

    private QueueSingleton(Context context) {
        QueueSingleton.ctx = context;
        this.requestQueue = this.getRequestQueue();
    }

    /**
     * Return instance of queue.
     * If queue doesn't exist return new one else return existing one.
     */
    public RequestQueue getRequestQueue() {
        if (this.requestQueue == null)
            this.requestQueue = Volley.newRequestQueue(QueueSingleton.ctx.getApplicationContext());
        return this.requestQueue;
    }

    /**
     * Add client request to queue.
     * @param req Request to server
     */
    public <T> void addToRequestQueue(Request<T> req) {
        this.getRequestQueue().add(req);
    }
}
