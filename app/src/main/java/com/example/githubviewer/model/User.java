package com.example.githubviewer.model;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

public class User implements Parcelable {

    public String login;
    public String name;
    public String company;
    public String location;
    public String email;
    public int reposCount;
    public int followersCount;
    public String creationDate;
    public String bio;
    public Bitmap avatar;

    public User setAvatar(Bitmap avatar) {
        this.avatar = avatar;
        return this;
    }

    public User setBio(String bio) {
        this.bio = bio;
        return this;
    }

    public User setName(String name) {
        this.name = name;
        return this;
    }

    public User setCompany(String company) {
        this.company = company;
        return this;
    }

    public User setLocation(String location) {
        this.location = location;
        return this;
    }

    public User setEmail(String email) {
        this.email = email;
        return this;
    }

    public User setReposCount(int reposCount) {
        this.reposCount = reposCount;
        return this;
    }

    public User setFollowersCount(int followersCount) {
        this.followersCount = followersCount;
        return this;
    }

    public User setCreationDate(String creationDate) {
        this.creationDate = creationDate;
        return this;
    }

    public User(String login) {
        this.login = login;
    }

    public User(Parcel in) {
        String[] data = new String[1];

        in.readStringArray(data);
        // the order needs to be the same as in writeToParcel() method
        this.login = data[0];
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringArray(new String[] {this.login});
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        public User[] newArray(int size) {
            return new User[size];
        }
    };
}
