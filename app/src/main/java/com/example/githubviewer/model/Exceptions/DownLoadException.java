package com.example.githubviewer.model.Exceptions;

public class DownLoadException extends Exception {

    private final String stdMessage = "Ops, something wrong happened!";

    String message = null;


    public DownLoadException() {
    }

    public DownLoadException(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return this.message == null ? this.stdMessage : this.message;
    }
}
