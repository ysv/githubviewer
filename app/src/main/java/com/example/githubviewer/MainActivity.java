package com.example.githubviewer;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.example.githubviewer.model.Exceptions.DownLoadException;
import com.example.githubviewer.model.GithubRepository;
import com.example.githubviewer.model.GithubRepository.ResponseListener;
import com.example.githubviewer.model.User;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity
        implements ResponseListener, ListFragment.OnListFragmentInteractionListener{

    private static final String AGS_USERS = "MainActivity_USERS";
    private static final String LIST_TAG = "MainActivity_LIST_FRAGMENT";

    private GithubRepository repo;
    private ArrayList<User> users = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        System.out.println(this.getClass().getName() + ": in onCreate()");
        setContentView(R.layout.activity_main);

        this.repo = GithubRepository.getInstance(this, this);

        if (savedInstanceState != null){
            this.users = savedInstanceState.getParcelableArrayList(MainActivity.AGS_USERS);
        } else {
            this.repo.nextUsers();
        }

    }

    @Override
    public void onUsersResponse(ArrayList<User> users) {
        if (this.users.isEmpty()) {
            this.users.addAll(users);
            ListFragment listFragment = ListFragment.newInstance(this.users);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, listFragment, MainActivity.LIST_TAG).commit();
        } else {
            int startIndex = this.users.size();
            this.users.addAll(users);
            Fragment fragment = getSupportFragmentManager().findFragmentByTag(MainActivity.LIST_TAG);
            if (fragment != null)
                if (fragment instanceof ListFragment)
                    ((ListFragment) fragment).notifyUsersInserted(startIndex, users.size());
        }
    }

    @Override
    public void onUserResponse(User user) {
        DetailFragment fragment = DetailFragment.newInstance(user);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, fragment).addToBackStack(null).commit();
    }

    @Override
    public void onListFragmentInteraction(User user) {
        // Creation date exist at any user. So if it is present there is no need to load user.
        if (user.creationDate != null) {
            this.onUserResponse(user);
        }
        // user is empty
        else
            this.repo.loadUser(user);
    }


    @Override
    public void onError(DownLoadException e) {
        Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_SHORT).show();
    }


    @Override
    public void loadNext() {
        this.repo.nextUsers();
    }


    @Override
    protected void onStop() {
        this.repo.cancelRequests();
        super.onStop();
        System.out.println(this.getClass().getName() + ": in onStop()");
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (this.users != null){
            outState.putParcelableArrayList(MainActivity.AGS_USERS, this.users);
        }
    }

}
