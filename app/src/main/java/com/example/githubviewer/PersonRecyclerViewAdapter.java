package com.example.githubviewer;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.githubviewer.ListFragment.OnListFragmentInteractionListener;
import com.example.githubviewer.model.User;

import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link User} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 */
public class PersonRecyclerViewAdapter extends RecyclerView.Adapter<PersonRecyclerViewAdapter.ViewHolder> {

    private final List<User> mValues;
    private final OnListFragmentInteractionListener mListener;

    public PersonRecyclerViewAdapter(List<User> items, OnListFragmentInteractionListener listener) {
        this.mValues = items;
        this.mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_person, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mUser = mValues.get(position);
        holder.mloginView.setText(holder.mUser.login);
        if (holder.mAvatar != null)
            holder.mAvatar.setImageBitmap(holder.mUser.avatar);
        if (this.mValues.size() - position < 3)
            this.mListener.loadNext();

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mUser);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;            // Visual representation of view
        public final TextView mloginView;
        public final ImageView mAvatar;
        public User mUser;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mloginView = (TextView) view.findViewById(R.id.user_login);
            mAvatar = (ImageView) view.findViewById(R.id.card_avatar);
        }

    }
}
