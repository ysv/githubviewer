package com.example.githubviewer;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.githubviewer.model.User;


public class DetailFragment extends Fragment {

    private static final String ARG_USER = "DetailFragment_USER";
    private static final String ARG_USER_SAVE = "DetailFragment_USER_SAVE";

    private User user;

    public DetailFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment DetailFragment.
     */
    public static DetailFragment newInstance(User user) {
        DetailFragment fragment = new DetailFragment();
        Bundle args = new Bundle();
        args.putParcelable(DetailFragment.ARG_USER, user);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // if fragment is created
        if (getArguments() != null) {
            this.user = getArguments().getParcelable(DetailFragment.ARG_USER);
        }
    }

    private void updateInfo(User user) {
        ((TextView) getView().findViewById(R.id.login_text)).setText(user.login);
        ((TextView) getView().findViewById(R.id.name_text)).setText(user.name);
        ((TextView) getView().findViewById(R.id.comp_text)).setText(user.company);
        ((TextView) getView().findViewById(R.id.location_text)).setText(user.location);
        ((TextView) getView().findViewById(R.id.email_text)).setText(user.email);
        ((TextView) getView().findViewById(R.id.repos_text)).setText(Integer.toString(user.reposCount));
        ((TextView) getView().findViewById(R.id.foll_text)).setText(Integer.toString(user.followersCount));
        ((TextView) getView().findViewById(R.id.date_text)).setText(user.creationDate);
        ((TextView) getView().findViewById(R.id.bio_text)).setText(user.bio);
        if (user.avatar != null)
            ((ImageView) getView().findViewById(R.id.detail_image)).setImageBitmap(user.avatar);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (savedInstanceState != null)
            this.user = savedInstanceState.getParcelable(ARG_USER_SAVE);

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        if (this.user != null)
            this.updateInfo(this.user);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(ARG_USER_SAVE, this.user);
    }
}
